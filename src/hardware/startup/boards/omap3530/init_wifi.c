/*
 * Written by Claudio L. and Freddy Martens
 */

#include "startup.h"
#include <arm/omap2420.h>
#include <arm/omap3530.h>

#define RSTDELAY					10000

/* GPIO pin 94 holds the power down (NPD) pin for the WIFI module and
 * GPIO pin 95 holds the reset (NRESET) pin. Both pins are active low.
 * GPIO pin 94 = GPIO3 bit 30
 * GPIO pin 95 = GPIO3 bit 31
 */
#define OMAP35XX_GPIO3_OE		    0x49052034	// This register is used to enable the pins output capabilities.
#define OMAP35XX_GPIO3_DATAOUT	    0x4905203C	//This register is used for setting the value of the GPIO output pins
#define OMAP35XX_GPIO3_SETDATAOUT	0x49052094	//This register is used for setting the value of the GPIO output pins
#define OMAP35XX_GPIO3_CLEARDATAOUT	0x49052090	//This register is used for clearing the value of the GPIO output pins

#define GPIO_WIFI_NPD     			30			// Power down (active low)
#define GPIO_WIFI_NRESET  			31			// reset (active low)

#define CONTROL_DEVCONF1 			0x480022D8
#define MMCSDIO2ADPCLKISEL  		(1<<6)
#define MMC2_MMCHS_SYSCONFIG 		0x480b4010
#define MMC2_MMCHS_SYSSTATUS 		0x480b4014
#define MMC_SOFTRESET 				0x2
#define MMC_RESETDONE 				0x1

void
init_wifi()
{
	int count = 10000;

	/* Select CLKS input for MMCSD2 */
	out32(CONTROL_DEVCONF1, in32(CONTROL_DEVCONF1) | MMCSDIO2ADPCLKISEL);

	/* Soft reset MMC2 host controller */
	out32(MMC2_MMCHS_SYSCONFIG, in32(MMC2_MMCHS_SYSCONFIG) | MMC_SOFTRESET);

	/* Wait until controller reset is complete */
	while( !(in32(MMC2_MMCHS_SYSSTATUS)&MMC_RESETDONE) );

	/* Power up the Marvell 80w8686 */
	/* GPIO set direction to output */
	/* nPD (power down) is active LOW. set HIGH to power up the mv8686 */
	out32(OMAP35XX_GPIO3_DATAOUT, in32(OMAP35XX_GPIO3_SETDATAOUT) | (1 << GPIO_WIFI_NPD));
	out32(OMAP35XX_GPIO3_OE, in32(OMAP35XX_GPIO3_OE) & ~(1 << GPIO_WIFI_NPD));

	/* Reset the WIFI and bluetooth module, this is suggested by ISEE tech support.     */
	/* GPIO set direction to output */
	/* nRESET is active LOW. set HIGH to release reset */
	out32(OMAP35XX_GPIO3_DATAOUT, in32(OMAP35XX_GPIO3_SETDATAOUT) | (1 << GPIO_WIFI_NRESET));
	out32(OMAP35XX_GPIO3_OE, in32(OMAP35XX_GPIO3_OE) & ~(1 << GPIO_WIFI_NRESET));

	/* Put a pulse on the nRESET to reset the Marvell adapter */
	out32(OMAP35XX_GPIO3_DATAOUT, in32(OMAP35XX_GPIO3_CLEARDATAOUT) | (1 << GPIO_WIFI_NRESET));
	while(count--) in32(OMAP35XX_GPIO3_DATAOUT);
	/* Clear the reset */
	out32(OMAP35XX_GPIO3_DATAOUT, in32(OMAP35XX_GPIO3_SETDATAOUT) | (1 << GPIO_WIFI_NRESET));
}
