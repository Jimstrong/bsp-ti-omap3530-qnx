###########################################################################
##
## QNX Neutrino 6.4.1 on the Texas Instruments OMAP 3530 Board
##
###########################################################################
##
## SUPPORTED DEVICES:
##
## SERIAL:   OMAP UART
## I2C:      Inter-Integrated Circuit Bus
## MMC/SD:   MultMediaCard/Secure Digital  
## USB OTG:   USB OTG Host Controller driver
## USB EHCI Host: USB EHCI Host Controller driver
## NAND:     ETFS Micron NAND
## AUDIO:    Audio Codec on TWL4030
## GRAPHICS: OMAP3530 output HDMI
## WIFI:  SDIO WIFI  
## SGX:  Experimental support for SGX GPU requires tools patch 
##
##  - For detailed instructions on the default example configuration for
##    these devices see the "CONFIGURING ON-BOARD SUPPORTED HARDWARE"
##    section below the build script section, or refer to the BSP docs.
##  - Tip: Each sub-section which relates to a particular device is marked
##         with its tag (ex. SERIAL). You can use the search features of
##         your editor to quickly find and add or remove support for
##         these devices.
##  - For details on how build files work, please refer to the help
##    documentation for "mkifs" in the QNX Neutrino "Utilities Reference".
## 
###########################################################################
##
## NOTES:
##
###########################################################################

###########################################################################
## START OF BUILD SCRIPT
###########################################################################

[image=0x80100000]
[+compress]

# For loading with the QNX IPL, uncomment below
# [virtual=armle,binary] .bootstrap = {

# For loading IFS images with the U-Boot
[virtual=armle,raw] .bootstrap = {
	# Startup for Dsplink config 2M.
        #
        # Ensure that board revision is set to beagle.
        #
	startup-omap3530 -L 0x87E00000,0x200000 -v -D8250.0x49020000^2.0.48000000.16 beagle
	
	#######################################################################
	## PATH set here is the *safe* path for executables.
	## LD_LIBRARY_PATH set here is the *safe* path for libraries.
	##     i.e. These are the paths searched by setuid/setgid binaries.
	##          (confstr(_CS_PATH...) and confstr(_CS_LIBPATH...))
	#######################################################################
	PATH=:/proc/boot:/bin:/usr/bin LD_LIBRARY_PATH=:/proc/boot:/lib:/usr/lib:/lib/dll procnto-v6-instr -v
}
[+script] .script = {
	# Initialise the console
	procmgr_symlink ../../proc/boot/libc.so.3 /usr/lib/ldqnx.so.2

	display_msg Welcome to QNX Neutrino 6.4.1 on the Texas Instruments OMAP3530 (ARMv7 Cortex-A8 core) - Beagle Board

	# Seed hardware resources into the Resource database manager 
	resource_seed
	
	#######################################################################
	## SERIAL driver
	##
	## Note: The beagle board does not have cts/rts connected so
	##       hardware flow control is disabled.
	#######################################################################
	devc-seromap -e -F -b115200 -c48000000/16 0x49020000^2,74
	waitfor /dev/ser1 4
	reopen /dev/ser1

	# UART3
#	devc-seromap -E -F -b38400 -c48000000/16 0x49020000^2,74
#	waitfor /dev/ser1 4
	
	# UART2
	devc-seromap -E -F -b38400 -c48000000/16 0x4806C000^2,73
	waitfor /dev/ser2 4
	
	# UART1
	devc-seromap -E -F -b38400 -c48000000/16 0x4806A000^2,72
	waitfor /dev/ser3 4
		
   	# Start some common servers
   	slogger &
   	pipe &

	#######################################################################
	## I2C driver
	##  - Required for both USB and AUDIO
	#######################################################################
	display_msg starting I2C driver...
	
	i2c-omap35xx
	waitfor /dev/i2c0

	#######################################################################
	## PMIC(TW4030) power management support utility
    	##  - It requires I2C driver 
    	#######################################################################
    	display_msg Configure power management chip 
    	pmic_tw4030_cfg
    
	#######################################################################
	## NAND_ETFS driver
	##
	## Provide proper "-r" value for reserveing the bootloader area on your board 
	##    -r kbytes              Set size of raw partition /dev/etfs1. Default: 0.
	## Only when starting the driver for the first time,
	##    use "-e" option to erase and create the ETFS filesystem
	#######################################################################
#	display_msg Starting etfs driver...
	#fs-etfs-omap3530_micron -r65536 -e -m /fs/etfs
#	fs-etfs-omap3530_micron -r65536 -m /fs/etfs

	#######################################################################
	## AUDIO driver
	##  - Requires the I2C driver to be enabled
	#######################################################################
#	display_msg Starting audio driver...
	#
#	io-audio -domap35xx-twl4030
#	waitfor /dev/snd/pcmC0D1p

	#######################################################################
	## USB OTG Host Controller driver and USB EHCI Host Controller driver
        ##  - Requires pmic_tw4030_cfg and I2C to be enabled
	#######################################################################
	display_msg Starting USB EHCI Host and OTG host driver...
	
	io-usb -dehci-omap3 ioport=0x48064800,irq=77  -domap3530-mg ioport=0x480ab000,irq=92
	waitfor /dev/io-usb/io-usb 4

	#######################################################################
	## WIFI start ram filesystem 
        ## - must be done before MMC start to ensure that hd0 is always used
	#######################################################################
	display_msg Starting RAM filesystem driver
	devb-ram ram capacity=16384,nodinit,cache=0m
	waitfor /dev/hd0
	fdisk /dev/hd0 add -t 6
	mount -e /dev/hd0
	mkdosfs /dev/hd0t6
	mount -t dos /dev/hd0t6 /fsram
	
	#######################################################################
	## MMC/SD driver
	##  - Requires the I2C driver to be enabled
	#######################################################################
	display_msg Starting MMC/SD driver...
#	devb-mmcsd-beagle cam quiet blk cache=2m mmcsd noac12
#	devb-mmcsd-beagle cam quiet blk cache=2m,automount=hd1t6:/fs/sd mmcsd noac12 dos exe=all
	###devb-mmcsd-beagle cam quiet blk cache=2m,automount=hd1t11:/fs/sd mmcsd noac12 dos exe=all
	devb-mmcsd-beagle cam quiet blk cache=2m,automount=hd1t11:/ mmcsd noac12 dos exe=all
	
	#######################################################################
	## SPI driver
	##  - Required for TSC
	#######################################################################
#	 display_msg starting SPI driver...
	#
	 spi-master -d omap3530
	 waitfor /dev/spi0
	 
	#######################################################################
	## GRAPHICS driver
	#######################################################################
#	 display_msg Starting Graphics driver... 
#	 io-display  -dvid=0,did=0
#	 waitfor /dev/io-display 10

	#######################################################################
   	## SGX GPU driver
	## Note:  the Composition_Manager patch should be install first
   	#######################################################################
 	# display_msg Starting SGX GPU driver... 
   	# GRAPHICS_ROOT=/usr/lib/graphics/omap3530
   	# /usr/lib/graphics/omap3530/pvrsrvd

	devc-serusb
	sleep 1
	#######################################################################
	## SDIO WIFI driver
	## Note: please insert SDIO WIFI card. and then run ". ./root/wifi.sh"  
	#######################################################################
        # required by supplicant
	random -p
	
#	io-pkt-v4 -d /proc/boot/devnp-mv8686-mmc2.so dir=/root/io-pkt,verbose=1,poll=0 -p tcpip mclbytes=4096,stacksize=65000 random
#	waitfor /dev/mv0 5
#	wpa_supplicant -Dwext -imv0 -c /etc/wpa_supplicant.conf &
#	wpa_supplicant -Dwext -imv0 -c /fs/sd/config/wpa_supplicant.conf &
#	sleep 1
#	ifconfig mv0 192.168.120.3

	/usr/sbin/dumper -d /dumps &
	#./fs/sd/config/wifi.sh
	./config/wifi.sh
	sleep 1
	qconn &
	inetd
	devc-pty &

	
	#######################################################################
	## These env variables are inherited by all the programs which follow
	#######################################################################
	SYSNAME=nto
	TERM=qansi
	HOME=/
	PATH=:/proc/boot:/bin:/usr/bin:/opt/bin
	LD_LIBRARY_PATH=:/proc/boot:/lib:/usr/lib:/lib/dll:/opt/lib

	[+session] sh &
	
#	sleep 60
#	display_msg Onboard nusuav is going to run!!
	##./fs/sd/nusuav
	###./nusuav
	/etc/rc.d/rc.local
}

# Redirect console messages
[type=link] /bin/sh=/proc/boot/ksh
[type=link] /dev/console=/dev/ser1
[type=link] /tmp=/dev/shmem

# Programs require the runtime linker (ldqnx.so) to be at a fixed location

# Shared libraries
libc.so

###########################################################################
## uncomment for legacy binary support (Momentics 6.3.2 and earlier)  
###########################################################################
libc.so.2

## Block device
libcam.so
io-blk.so
cam-disk.so
fs-qnx4.so
fs-dos.so

###########################################################################
## uncomment for USB driver
###########################################################################
libusbdi.so
devu-omap3530-mg.so
devu-ehci-omap3.so

###########################################################################
## uncomment for SPI
###########################################################################
 spi-omap3530.so

###########################################################################
## uncomment for graphics driver
###########################################################################
#devg-omap35xx.so
#libGLES_CL.so
#libGLES_CM.so
#libffb.so
#libgf.so
#devg-soft3d-fixed.so
#libFF-T2K.so.2
libm.so.2
#devg-soft3d.so

###########################################################################
## SGX support
## Note:  the Composition_Manager patch must be installed for SGX support
##   Refer to the BSP release notes for how to install the patch into the BSP.
###########################################################################
# /usr/lib/graphics/omap3530/libglslcompiler.so=../prebuilt/armle/usr/lib/graphics/omap3530/libglslcompiler.so
# /usr/lib/graphics/omap3530/libIMGegl.so=../prebuilt/armle/usr/lib/graphics/omap3530/libIMGegl.so
# /usr/lib/graphics/omap3530/libImgGLESv1_CM.so=../prebuilt/armle/usr/lib/graphics/omap3530/libImgGLESv1_CM.so
# /usr/lib/graphics/omap3530/libImgGLESv2.so=../prebuilt/armle/usr/lib/graphics/omap3530/libImgGLESv2.so
# /usr/lib/graphics/omap3530/libImgOpenVG.so=../prebuilt/armle/usr/lib/graphics/omap3530/libImgOpenVG.so
# /usr/lib/graphics/omap3530/libpvr2d.so=../prebuilt/armle/usr/lib/graphics/omap3530/libpvr2d.so
# /usr/lib/graphics/omap3530/libsrv_um.so=../prebuilt/armle/usr/lib/graphics/omap3530/libsrv_um.so
# /usr/lib/graphics/omap3530/libWFDdevg.so=../prebuilt/armle/usr/lib/graphics/omap3530/libWFDdevg.so
# /usr/lib/graphics/omap3530/pvrsrvinit.so=../prebuilt/armle/usr/lib/graphics/omap3530/pvrsrvinit.so
# /usr/lib/graphics/omap3530/wsegl-gf.so=../prebuilt/armle/usr/lib/graphics/omap3530/wsegl-gf.so

###########################################################################
## The following libraries are taken from the Composition_Manager patch.
##   Refer to the BSP release notes for how to install the patch into the BSP.
###########################################################################
# /usr/lib/graphics/devg/libEGLdevg.so=../prebuilt/armle/usr/lib/graphics/devg/libEGLdevg.so
# /usr/lib/graphics/devg/libWFDdevg.so=../prebuilt/armle/usr/lib/graphics/devg/libWFDdevg.so
# /usr/lib/graphics/devg/libGLESv1_CLdevg.so=../prebuilt/armle/usr/lib/graphics/devg/libGLESv1_CLdevg.so
# /usr/lib/graphics/devg/libGLESv1_CMdevg.so=../prebuilt/armle/usr/lib/graphics/devg/libGLESv1_CMdevg.so
# libiow.so.1
# libGLESv1_CM.so.1
# libGLESv1_CL.so.1
# libEGL.so.1

###########################################################################
## uncomment for AUDIO driver
###########################################################################
deva-ctrl-omap35xx-twl4030.so
libasound.so.2

###########################################################################
## uncomment for network driver
###########################################################################
devn-asix.so
devnp-shim.so

###########################################################################
## uncomment for WIFI driver
###########################################################################
# devnp-mv8686.so
devnp-mv8686-mmc2.so

libcrypto.so.1
libssl.so.1
libsdio.so.1
libsocket.so.2
libsocket.so.3
libcam.so.2
libm.so.2
libz.so.2

# Executables
[data=c]
devc-seromap

###########################################################################
## uncomment for ETFS driver
###########################################################################
fs-etfs-omap3530_micron
etfsctl

###########################################################################
## uncomment for SD driver
###########################################################################
devb-mmcsd-beagle

###########################################################################
## uncomment for USB driver
###########################################################################
io-usb
usb
devb-umass

###########################################################################
## uncomment for I2C driver
###########################################################################
i2c-omap35xx

###########################################################################
## uncomment for SPI driver
###########################################################################
spi-master

###########################################################################
## Power management utility
###########################################################################
pmic_tw4030_cfg

###########################################################################
## uncomment for AUDIO driver
###########################################################################
io-audio
wave
waverec
mix_ctl

###########################################################################
## uncomment for Graphics driver
###########################################################################
vsync
egl-gears-lite
io-display
/etc/system/config/omap35xx.conf=omap35xx.conf
/etc/system/config/display.conf={
device \{
       drivername=omap35xx
        vid=0
        did=0
       modeopts=/etc/system/config/omap35xx.conf
        display \{
              xres=720
              yres=400
              refresh=60
              pixel_format=rgb565
        \}
    \}
}
###########################################################################
## uncomment for SGX Graphics driver
##   The composition manager patch must be installed to use thse binaries.
###########################################################################
# /usr/lib/graphics/omap3530/graphics.conf=../prebuilt/armle/usr/lib/graphics/omap3530/graphics.conf
# /usr/lib/graphics/omap3530/pvrsrvd=../prebuilt/armle/usr/lib/graphics/omap3530/pvrsrvd
# /usr/lib/graphics/omap3530/gles1-egl-gears=../prebuilt/armle/usr/lib/graphics/omap3530/gles1-egl-gears

###########################################################################
## uncomment for WIFI driver
###########################################################################
#/root/wifi.sh={
	## WIFI driver ##
#	io-pkt-v4 -d /proc/boot/devnp-mv8686-mmc2.so dir=/root/io-pkt,verbose=1,poll=0 -p tcpip mclbytes=4096,stacksize=65000 random
#	ifconfig mv0 up
#	wpa_supplicant  -Dwext -imv0 -C /fsram/tmpfs &
#	wpa_cli -i mv0 -p /fsram/tmpfs
#}
/root/io-pkt/helper_sd.bin=helper_sd.bin
/root/io-pkt/sd8686.bin=sd8686.bin
helper_sd.bin
sd8686.bin

/etc/wpa_supplicant.conf={
ctrl_interface=/fsram/tmpfs
eapol_version=1
ap_scan=2
network={
	ssid="gumstix120"
	wep_key0=1234567890
	key_mgmt=NONE
	mode=1
	\}
}
/etc/inetd.conf=$(QNX_TARGET)/etc/inetd.conf
/etc/passwd=$(QNX_TARGET)/etc/passwd

wpa_supplicant
wpa_cli
devb-ram 
fdisk 
mount 
mkdosfs 
random
inetd
/usr/sbin/telnetd=telnetd
###########################################################################
## uncomment for NETWORK driver
###########################################################################
 io-pkt-v4
 nicinfo
 ping
 fs-nfs2
 ifconfig
 dhcp.client
 qconn
 
## shared libraries to be used
libcpp.so.2
libcpp.so.2a
libcpp.so.3
libcpp.so.4
libstdc++.so.5
libstdc++.so.5.0.7
libstdc++.so.6
libstdc++.so.6.0.9
libstdc++.so.6.0.13

#libdsplink160-client.a

###########################################################################
## general commands
###########################################################################
resource_seed
ls
mv
cat
grep
more
mount
ksh
pipe
pidin
uname
umount
slogger
sloginfo
slay
cp
rm
waitfor
shutdown
sleep
chmod
use
ln
hostname
pdebug
devc-pty
ldd
devc-serusb
mkdir

#dsplink160
#dsplink160-test-Jacinto
libOpenCV-2.3.1.so
libOpenCV-2.3.1.so.1
libflexxes.so
###########################################################################
## END OF BUILD SCRIPT
###########################################################################

###########################################################################
##
## CONFIGURING ON-BOARD SUPPORTED HARDWARE:
##
##-------------------------------------------------------------------------
##
## SERIAL: OMAP UART
##   example buildfile commands:
##   devc-seromap -e -F -b115200 -c48000000/16 0x49020000^2,74
##     waitfor /dev/ser1
##     reopen /dev/ser1
##
##   required binaries:
##     devc-seromap
##
##-------------------------------------------------------------------------
##
## MMC/SD: 
##   example buildfile commands:
##   devb-mmcsd-beagle cam quiet blk cache=2m mmcsd noac12
##
##   required binaries:
##     devb-mmcsd-beagle
##
##   NOTE: i2c is required for MMC operation and should be started before MMC.
##
##-------------------------------------------------------------------------
## 
## NAND FLASH: Micron NAND
##    example buildfile commands:
##    fs-etfs-omap3530_micron -r65536 -e -m /fs/etfs (First time only)
##    fs-etfs-omap3530_micron -r65536 -m /fs/etfs
##
##    required binaries:
##      fs-etfs-omap3530_micron
##      etfsctl
##
##    To erase and format the NAND flash partition:
##        etfsctl -d /dev/etfs2 -S -e
##        etfsctl -d /dev/etfs2 -S -f -c
##    You should slay and restart the driver,
##    you should now see the mount point /fs/etfs/ which can be used to copy files to.
##
##-------------------------------------------------------------------------
##
## I2C:     Inter-Integrated Circuit Bus
##   example buildfile commands:
##     i2c-omap35xx
##
##   required binaries:
##     i2c-omap35xx
##
##-------------------------------------------------------------------------
##
## AUDIO:   Audio codec on the TWL4030 (playback and capture support)
##
##   example buildfile commands:
##     io-audio -domap35xx-twl4030
##
##   required libraries:
##     deva-ctrl-omap35xx-twl4030.so
##     libasound.so
##     
##   required binaries:
##     resource_seed
##     io-audio
##
##   NOTE: resource_seed and pmic_tw4030_cfg must be run before io-audio
##
##-------------------------------------------------------------------------
##
## USB EHCI:     USB EHCI HOST driver
##   example buildfile commands:
##     	io-usb -dehci-omap3 ioport=0x48064800,irq=77,verbose=5 
##
##   required binaries:
##     	devu-ehci-omap3.so
##		libusbdi.so
##		io-usb
##		usb
##
##   NOTE: pmic_tw4030_cfg must be run before io-usb for the EHCI driver
##
##-------------------------------------------------------------------------
##
## USB OTG:     USB OTG HOST driver
##   example buildfile commands:
##      io-usb -domap3530-mg ioport=0x480ab000,irq=92
##
##   required binaries:
##              devu-omap3530-mg.so
##		libusbdi.so
##		io-usb
##		usb
##
##  NOTE: For OTG to function properly, beagle board must be reset with the
##        USER button held down to force it to attempt to boot from USB and
##        configure the OTG.
##		
##-------------------------------------------------------------------------
##
## GRAPHICS: OMAP3530 output HDMI
##   example buildfile commands:
##              io-display  -dvid=0,did=0
##
##   required binaries:
##              devg-omap35xx.so
##              libGLES_CL.so
##              libffb.so
##              libgf.so
##              devg-soft3d-fixed.so
##              libFF-T2K.so.2
##              libGLES_CM.so
##              libm.so.2
##              devg-soft3d.so
##
##   The following configuration file is required to initialize the
##   display device:
##
##     /etc/system/config/omap35xx.conf=omap35xx.conf
##     /etc/system/config/display.conf={
##       device \{
##         drivername=omap35xx
##         vid=0
##         did=0
##         modeopts=/etc/system/config/omap35xx.conf
##         display \{
##           xres=720
##           yres=400
##           refresh=60
##           pixel_format=rgb565
##        \}
##      \}
##     }
##
##-------------------------------------------------------------------------
##
## SGX GPU SUPPORT
##   Please refer to the Beagle BSP Release notes for how to install
##   the composition manager patch, which supplies many of the required
##   libraries and binaries.
##
##   example buildfile commands:
##     GRAPHICS_ROOT=/usr/lib/graphics/omap3530
##     /usr/lib/graphics/omap3530/pvrsrvd
##
##   The following configuration file is required to initialize the
##   display device:
##     /usr/lib/graphics/omap3530/graphics.conf
##
##   required binaries:
##     pvrsrvd
##     gles1-egl-gears
##     libglslcompiler.so
##     libIMGegl.so
##     libImgGLESv1_CM.so
##     libImgGLESv2.so
##     libImgOpenVG.so
##     libpvr2d.so
##     libsrv_um.so
##     libWFDdevg.so
##     pvrsrvinit.so
##     wsegl-gf.so
##     libEGLdevg.so
##     libWFDdevg.so
##     libGLESv1_CLdevg.so
##     libGLESv1_CMdevg.so
##     libiow.so.1
##     libGLESv1_CM.so.1
##     libGLESv1_CL.so.1
##     libEGL.so.1
##
##-------------------------------------------------------------------------
##
## WIFI:
##   Please refer to the WIFI_HOW_TO_RUN.txt file within the BSP for
##   instructions on configuring the WIFI.
##
###########################################################################

###########################################################################
##
## INTERRUPT MAP
##
###########################################################################
##
## vector:   0
## trigger:  level
## device:   EMUINT (MPU emulation)
##
## vector:   1
## trigger:  level
## device:   COMMTX (MPU emulation)
##
## vector:   2
## trigger:  level
## device:   COMMRX (MPU emulation)
##
## vector:   3 
## trigger:  level
## device:   BENCH (MPU emulation)
##
## vector:   4
## trigger:  level
## device:   MCBSP2_ST_IRQ (Sidetone MCBSP2 overflow)
##
## vector:   5
## trigger:  level
## device:   MCBSP3_ST_IRQ (Sidetone MCBSP3 overflow)
##
## vector:   6
## trigger:  level
## device:   SSM_ABORT_IRQ (MPU subsystem secure state-machine abort)
##
## vector:   7
## trigger:  level
## device:   sys_nirq (External source - active low)
##
## vector:   8
## trigger:  N/A
## device:   Reserved
##
## vector:   9
## trigger:  level
## device:   SMX_DBG_IRQ (SMX error for debug)
##
## vector:   10
## trigger:  level
## device:   SMX_APP_IRQ SMX error for application
##
## vector:   11
## trigger:  level
## device:   PRCM_MPU_IRQ PRCM module IRQ
##
## vector:   12
## trigger:  level
## device:   SDMA_IRQ0 System DMA request 0
##
## vector:   13
## trigger:  level
## device:   SDMA_IRQ1 System DMA request 1
##
## vector:   14
## trigger:  level
## device:   SDMA_IRQ2 System DMA request 2
##
## vector:   15
## trigger:  level
## device:   SDMA_IRQ3 System DMA request 3
##
## vector:   16
## trigger:  level
## device:   MCBSP1_IRQ McBSP module 1 IRQ
##
## vector:   17
## trigger:  level
## device:   MCBSP2_IRQ McBSP module 2 IRQ
##
## vector:   18
## trigger:  level
## device:   SR1_IRQ SmartReflex 1
##
## vector:   19
## trigger:  level
## device:   SR2_IRQ SmartReflex 2
##
## vector:   20
## trigger:  level
## device:   GPMC_IRQ General-purpose memory controller module
##
## vector:   21
## trigger:  level
## device:   SGX_IRQ 2D/3D graphics module
##
## vector:   22
## trigger:  level
## device:   MCBSP3_IRQ McBSP module 3
##
## vector:   23
## trigger:  level
## device:   MCBSP4_IRQ McBSP module 4
##
## vector:   24
## trigger:  level
## device:   CAM_IRQ0 Camera interface request 0
##
## vector:   25
## trigger:  level
## device:   DSS_IRQ Display subsystem module
##
## vector:   26
## trigger:  level
## device:   MAIL_U0_MPU_IRQ Mailbox user 0 request
##
## vector:   27
## trigger:  level
## device:   MCBSP5_IRQ McBSP module 5
##
## vector:   28
## trigger:  level
## device:   IVA2_MMU_IRQ IVA2 MMU
##
## vector:   29
## trigger:  level
## device:   GPIO1_MPU_IRQ GPIO module 1
##
## vector:   30
## trigger:  level
## device:   GPIO2_MPU_IRQ GPIO module 2
##
## vector:   31
## trigger:  level 
## device:   GPIO3_MPU_IRQ GPIO module 3
##
## vector:   32
## trigger:  level
## device:   GPIO4_MPU_IRQ GPIO module 4
##
## vector:   33
## trigger:  level
## device:   GPIO5_MPU_IRQ GPIO module 5
##
## vector:   34
## trigger:  level
## device:   GPIO6_MPU_IRQ GPIO module 6
##
## vector:   35
## trigger:  level
## device:   USIM_IRQ USIM interrupt (HS devices only)
##
## vector:   36
## trigger:  level
## device:   WDT3_IRQ Watchdog timer module 3 overflow
##
## vector:   37
## trigger:  level
## device:   GPT1_IRQ General-purpose timer module 1 
##
## vector:   38
## trigger:  level
## device:   GPT2_IRQ General-purpose timer module 2
##
## vector:   39
## trigger:  level
## device:   GPT3_IRQ General-purpose timer module 3
##
## vector:   40
## trigger:  level
## device:   GPT4_IRQ General-purpose timer module 4
##
## vector:   41
## trigger:  level
## device:   GPT5_IRQ General-purpose timer module 5
##
## vector:   42
## trigger:  level
## device:   GPT6_IRQ General-purpose timer module 6
##
## vector:   43
## trigger:  level
## device:   GPT7_IRQ General-purpose timer module 7
##
## vector:   44
## trigger:  level
## device:   GPT8_IRQ General-purpose timer module 8
##
## vector:   45
## trigger:  level
## device:   GPT9_IRQ General-purpose timer module 9
##
## vector:   46
## trigger:  level
## device:   GPT10_IRQ General-purpose timer module 10
##
## vector:   47
## trigger:  level
## device:   GPT11_IRQ General-purpose timer module 11
##
## vector:   48
## trigger:  level
## device:   SPI4_IRQ McSPI module 4
##
## vector:   49
## trigger:  level
## device:   SHA1MD5_IRQ2 SHA-1/MD5 crypto-accelerator 2 (HS devices only)
##
## vector:   50
## trigger:  N/A
## device:   FPKA_IRQREADY_N PKA crypto-accelerator (HS devices only)
##
## vector:   51
## trigger:  level
## device:   SHA2MD5_IRQ SHA-2/MD5 crypto-accelerator 1 (HS devices only)
##
## vector:   52
## trigger:  level
## device:   RNG_IRQ RNG module (HS devices only)
##
## vector:   53
## trigger:  level
## device:   MG_IRQ MG function
##
## vector:   54
## trigger:  level
## device:   MCBSP4_IRQ_TX McBSP module 4 transmit
##
## vector:   55
## trigger:  level
## device:   MCBSP4_IRQ_RX McBSP module 4 receive
##
## vector:   56
## trigger:  level
## device:   I2C1_IRQ I2C module 1
##
## vector:   57
## trigger:  level
## device:   I2C2_IRQ I2C module 2
##
## vector:   58
## trigger:  level
## device:   HDQ_IRQ HDQ/One-wire
##
## vector:   59
## trigger:  level
## device:   McBSP1_IRQ_TX McBSP module 1 transmit
##
## vector:   60
## trigger:  level
## device:   McBSP1_IRQ_RX McBSP module 1 receive
##
## vector:   61
## trigger:  level 
## device:   I2C3_IRQ I2C module 3
##
## vector:   62
## trigger:  level
## device:   McBSP2_IRQ_TX McBSP module 2 transmit
##
## vector:   63
## trigger:  level
## device:   McBSP2_IRQ_RX McBSP module 2 receive
##
## vector:   64
## trigger:  level
## device:   FPKA_IRQRERROR_N PKA crypto-accelerator (HS devices only)
##
## vector:   65
## trigger:  level
## device:   SPI1_IRQ McSPI module 1
##
## vector:   66
## trigger:  level
## device:   SPI2_IRQ McSPI module 2
##
## vector:   67
## trigger:  level
## device:   RESERVED
##
## vector:   68
## trigger:  level
## device:   RESERVED
##
## vector:   69
## trigger:  level
## device:   RESERVED
##
## vector:   70 
## trigger:  level
## device:   RESERVED
##
## vector:   71
## trigger:  level
## device:   RESERVED
##
## vector:   72
## trigger:  level
## device:   UART1_IRQ UART module 1
##
## vector:   73
## trigger:  level
## device:   UART2_IRQ UART module 2
##
## vector:   74
## trigger:  level 
## device:   UART3_IRQ UART module 3 (also infrared)
##
## vector:   75
## trigger:  level
## device:   PBIAS_IRQ Merged interrupt for PBIASlite1 and 2
##
## vector:   76
## trigger:  level
## device:   OHCI_IRQ OHCI controller HSUSB MP Host Interrupt
##
## vector:   77
## trigger:  level
## device:   EHCI_IRQ EHCI controller HSUSB MP Host Interrupt
##
## vector:   78
## trigger:  level
## device:   TLL_IRQ HSUSB MP TLL Interrupt
##
## vector:   79
## trigger:  level
## device:   PARTHASH_IRQ SHA2/MD5 crypto-accelerator 1 (HS devices only)
##
## vector:   80
## trigger:  level
## device:   RESERVED
##
## vector:   81
## trigger:  level
## device:   MCBSP5_IRQ_TX McBSP module 5 transmit
##
## vector:   82
## trigger:  level
## device:   MCBSP5_IRQ_RX McBSP module 5 receive
##
## vector:   83
## trigger:  level
## device:   MMC1_IRQ MMC/SD module 1
##
## vector:   84
## trigger:  level
## device:   MS_IRQ MS-PRO module
##
## vector:   85
## trigger:  level
## device:   RESERVED
##
## vector:   86
## trigger:  level
## device:   MMC2_IRQ MMC/SD module 2
##
## vector:   87
## trigger:  level
## device:   MPU_ICR_IRQ MPU ICR
##
## vector:   88
## trigger:  level
## device:   RESERVED 
##
## vector:   89
## trigger:  level
## device:   MCBSP3_IRQ_TX McBSP module 3 transmit
##
## vector:   90
## trigger:  level
## device:   MCBSP3_IRQ_RX McBSP module 3 receive 
##
## vector:   91
## trigger:  level
## device:   SPI3_IRQ McSPI module 3
##
## vector:   92
## trigger:  level
## device:   HSUSB_MC_NINT High-Speed USB OTG controller 
##
## vector:   93
## trigger:  level
## device:   HSUSB_DMA_NINT High-Speed USB OTG DMA controller
##
## vector:   94
## trigger:  level
## device:   MMC3_IRQ MMC/SD module 3 
##
## vector:   95
## trigger:  level
## device:   GPT12_IRQ General-purpose timer module 12
##
###########################################################################
##
## GPIO module 6 Interrupts
##
## vector:  160 - 191 (GPIO 160 - 191)
## trigger: user defined
## device:
##
###########################################################################
##
## SDMA 3 interrupt
##
## vector:  256 - 287 (dma channel 0 - 31)
## trigger: user defined
## device:
##
###########################################################################
