/*
 * $QNXLicenseC:
 * Copyright 2008, QNX Software Systems. 
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"). You 
 * may not reproduce, modify or distribute this software except in 
 * compliance with the License. You may obtain a copy of the License 
 * at: http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTIES OF ANY KIND, either express or implied.
 *
 * This file may contain contributions from others, either as 
 * contributors under the License or as licensors under other terms.  
 * Please review this entire file for other proprietary rights or license 
 * notices, as well as the QNX Development Suite License Guide at 
 * http://licensing.qnx.com/license-guide/ for other information.
 * $
 */




#include "startup.h"

/*
 * Additional Cortex A8 specific CPU initialisation
 */

void
armv_setup_a8(struct cpuinfo_entry *cpu, unsigned cpuid)
{
        unsigned    aux;

	/*
	 * Perform generic ARMv7 CPU initialisation
	 */
	armv_setup_v7(cpu, cpuid, 0);

	/*
	 * Set up Auxilary Control Register
         */
     	aux = arm_v6_cp15_auxcr_get();
     	aux |= (1 << 1);        // Enable L2 cache
     	aux |= (1 << 5);        // Errata #458693 - set L1NEON
     	aux |= (1 << 9);        // Errata #458693 - set PLDNOP
     	arm_v6_cp15_auxcr_set(aux);

}


__SRCVERSION( "$URL: http://svn/product/tags/public/bsp/nto641/ti-omap3530-beagle/latest/src/hardware/startup/lib/arm/armv_setup_a8.c $ $Rev: 308279 $" );
