/*
 * $QNXLicenseC:
 * Copyright 2009, QNX Software Systems. 
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"). You 
 * may not reproduce, modify or distribute this software except in 
 * compliance with the License. You may obtain a copy of the License 
 * at: http://www.apache.org/licenses/LICENSE-2.0 
 * 
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" basis, 
 * WITHOUT WARRANTIES OF ANY KIND, either express or implied.
 *
 * This file may contain contributions from others, either as 
 * contributors under the License or as licensors under other terms.  
 * Please review this entire file for other proprietary rights or license 
 * notices, as well as the QNX Development Suite License Guide at 
 * http://licensing.qnx.com/license-guide/ for other information.
 * $
 */

#include "startup.h"

/*
 * Detect various configurations for Marvell Sheeva processors
 */

const struct armv_chip *
armv_detect_sheeva(void)
{
	unsigned	cpuid_ext;

	__asm__ __volatile__("mrc	p15, 1, %0, c15, c12, 0" : "=r"(cpuid_ext));
	switch (cpuid_ext) {
	case 0x00020001:
		return &armv_chip_sheeva_v6;

	case 0x00030001:
		return &armv_chip_sheeva_v7;

	default:
		return 0;
	}
}


__SRCVERSION( "$URL$ $Rev$" );
